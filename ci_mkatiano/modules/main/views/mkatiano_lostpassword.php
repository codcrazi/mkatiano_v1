<!-- Libraries -->
    <? include 'ci_mkatiano/helpers/mk_header.php'; ?>
    <? include 'ci_mkatiano/helpers/login_js_css.php'; ?>
<!-- End of Libraries -->

</head>
    <body>
    <div class="wrap">
        <div id="content">
            <div id="main">
                <div class="full_w">
                    <p class="descr">Recover Mkatiano password. <br/> Fill in the form below and click on 'Reset password'</p>
                    
                    <form action="" method="post" id="resetPassForm">
                        <label for="mk_uemail">Email Address:</label>
                        <input id="mk_uemail" name="mk_uemail" class="text" />
                        
                        <div class="sep"></div>
                        
                        <center>
                            <button type="submit" class="add">Reset Password</button> 
                            <br/> <br/>
                            Already a member?<br/>
                            <a class="button ok" href="/login">Login</a>
                        </center>
                            
                    </form>
                </div>
                <div class="footer">&raquo; <a href="http://www.iddsalim.com/">By Idd Salim</a> </div>
            </div>
        </div>
    </div>
    </body>
</html>
