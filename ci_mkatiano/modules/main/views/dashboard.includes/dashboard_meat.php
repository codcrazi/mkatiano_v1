<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="pl" xml:lang="pl">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Paweł 'kilab' Balicki - kilab.pl" />
<title><?=$page_title;?></title>

    <? include 'ci_mkatiano/helpers/js_css.php'; ?>

</head>
<body>
<div class="wrap">
    <? include 'header.php'; ?>

	<div id="content">
    <? include 'sidebar.php'; ?>

		<div id="main">
                    <? include 'dashboard_section_'.$page_section.'.php'; ?>
                    <div class="clear"></div>
                </div>
            
	<div id="footer">
            <div class="left">
                &copy; <a href="http://www.iddsalim.com/" target="_new">Idd Salim</a> of <a href="http://www.xema.mobi/" target="_new">Xema Labs</a></p>
            </div>
            <div class="right">
                <p><a href="/credits">Credits</a> | <a href="/license">License</a></p>
            </div>
	</div>
</div>

</body>
</html>
