# Mkatiano Version 1.0

The system has the following pre-requisites:

To be shared with the developers:


The framework is based on 3 core server technologies:

-> Code-Igniter 2.1.2 Framework 
-> jQuery
-> MemCached 1.8.1 Community edition.



Pre-requisites:
-> Running Apache 2.2+ Server
-> PostGres 9.1 Server
-> PHP 5 with Postgres Apache modules.
-> MemCached 1.8.1 running on localhost
-> Clear understanding of CodeIgniter



Pre-training tasks:
-> Read and understand ALL about CodeIgniter and MVC - http://codeigniter.com/user_guide/
-> Understand how the CI HMVC works - https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc/wiki/Home
-> Working understanding of Postgres, apache.
-> Understanding of MemCached and, by extension, CouchBase [http://memcached.org/] then [http://www.couchbase.com/]
-> Understand the basics of jQuery - http://docs.jquery.com/
-> Install all the necessary software locally.



Warmest Regards, 
_________________ 
Cdr. Idd Salim Kithinji, 
Consultant, iHub Nairobi,
Founder, Coder (Web, Mobile, MobileWeb) 
Xema Labs Limited,
